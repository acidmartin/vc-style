# vc-style

> Dynamic `<style></style>` tag component for [VueJs](https://vuejs.org) making any property/value pairs inside of it observable. Developed by [Martin Ivanov](https://wemakesites.net/).

## Installation

```$ npm install vc-style --save```

## Usage

```
// in main.js, use globally
import VcStyle from 'vc-style'
Vue.use(VcStyle)

// as a component in another component
import VcStyle from 'vc-style'

export default {
  name: 'app',
  components: {
    VcStyle
  },
  data () {
    return {
      color: '#fff',  
      bgColor: '#b00'
    }
  }
}
```

And then use like this:

```
<vc-style>
body
{
  color: {{ color }};
  background: {{ bgColor }};
}
</vc-style>
```

The above will render normal `<style></style>` tag with all property/value pairs observable:

```
<style>
body
{
  color: #fff;
  background: #b00;
}
</style>
```

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Credits

Developed by [Martin Ivanov](https://wemakesites.net/) and available on [BitBucket](https://bitbucket.org/acidmartin/vc-style/) and on [NPM](https://www.npmjs.com/package/vc-style). Check the demo [here](https://vc-style.wemakesites.net/).